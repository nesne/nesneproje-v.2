#pragma once
/**
* @File RobotInterface.h
* @Date Aralik, 2018
* @Author Eda Ayaz (edayazz.1@gmail.com)
*
* \brief RobotInterface class
* \brief Bu class abstract bir classtir.
*/
#include "RangeSensor.h"
#include "Pose.h"
//#include "RobotControl.h"
#include "LaserSensor.h"
#include "SonarSensor.h"
class RobotInterface
{
private:
	int state;
	vector<RangeSensor*> sensors;
	Pose *position;
public:
	RobotInterface();
	RobotInterface(PioneerRobotAPI *robotAPI);
	/**
	* \brief Abstract sinifta Robotu sola donduren fonksiyon
	*/
	virtual void turnLeft() = 0;
	/**
	* \brief Abstract sinifta Robotu saga donduren fonksiyon
	*/
	virtual void turnRight() = 0;
	/**
	* \brief Robotun verilen hizda ilerlemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	virtual void forward(float speed) = 0;
	/**
	* \brief Konumu, laser ve sonar sensorleri konsola yazdirir
	*/
	virtual void print() = 0;
	/**
	* \brief Robotun verilen hizda gerilemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	virtual void backward(float speed) = 0;
	/**
	* \brief Pozisyonu almamizi saglayan fonksiyon
	* \return Pose* tipinde pozisyonu dondurur
	*/
	virtual Pose getPose() = 0;
	/**
	* \brief Pozisyona deger atamamizi saglayan fonksiyon
	* \param Pozisyon tutan Pose
	*/
	virtual void setPose(Pose) = 0;
	/**
	* \brief Robotun donusunu durduran fonksiyon
	*/
	virtual void stopTurn() = 0;
	/**
	* \brief Robotu durduran fonksiyon
	*/
	virtual void stopMove() = 0;
	/**
	* \brief   Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	*/
	virtual void updateSensor();
	/**
	* \brief pure virtual destructor
	*/
	virtual ~RobotInterface();

};