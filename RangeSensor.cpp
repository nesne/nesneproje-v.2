#include "RangeSensor.h"

RangeSensor::RangeSensor()
{
}

RangeSensor::RangeSensor(PioneerRobotAPI * robotAPI)
{
	this->robotAPI = robotAPI;
	
}

float RangeSensor::operator[](int i)
{
	return this->ranges[i];
}

RangeSensor::~RangeSensor()
{
}