#pragma once

#include <iostream>
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Path.h"
#include "Record.h"
#include "RobotOperator.h"
#include "RangeSensor.h"
#include "LaserSensor.h"
#include "SonarSensor.h"
//#include "RobotInterface.h"
#include "PioneerRobotInterface.h"
/**
* @File RobotControl.h
* @Date Aralik, 2018
* @Author Emre Koc (emrekoc950@gmail.com)
*
* \brief RobotControl class
* \brief Bu class robotun hareketlerinin kontrolunu saglar.
*/
using namespace std;
class RobotControl
{
private:
	vector<RangeSensor*>sensors;
	Path * path;
	Record *record;
	RobotOperator *robotOperator;
	Pose* position;
	PioneerRobotAPI* robotAPI;
	PioneerRobotInterface *robotInterface;
	bool accesState;
public:
	/**
	* \brief Robotu sola donduren fonksiyon
	*/
	void turnLeft();
	/**
	* \brief Robotu saga donduren fonksiyon
	*/
	void turnRight();
	/**
	* \brief Robotun verilen hizda ilerlemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void forward(float);
	/**
	* \brief Konumu, laser ve sonar sensorleri konsola yazdirir
	*/
	void print();
	/**
	* \brief Robotun verilen hizda gerilemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void backward(float);
	/**
	* \brief Pozisyonu almamizi saglayan fonksiyon
	* \return Pose* tipinde pozisyonu dondurur
	*/
	Pose getPose();
	/**
	* \brief Pozisyona deger atamamizi saglayan fonksiyon
	* \param Pozisyon tutan Pose
	*/
	void setPose(Pose);
	/**
	* \brief Robotun donusunu durduran fonksiyon
	*/
	void stopTurn();
	/**
	* \brief Robotu durduran fonksiyon
	*/
	void stopMove();
	/**
	* \brief Robotu robotun bulundugu konumu path’e ekler.
	*/
	bool addToPath();
	/**
	* \brief Path temzilenir.
	*/
	bool clearPath();
	/**
	* \brief Bu fonksiyon ile Path nesnesinde yuklenmis olan konumlar dosyaya yazdirilmaktadir.
	*/
	bool recordPathToFile();
	/**
	* \brief  Bu fonksiyon erisim icin kullanilmaktadir. Bu fonksiyon uzerinden, dogru sifre giris ister.
	*/
	bool openAccess(int);
	/**
	* \brief Bu fonksiyon erisimi bitirir.
	*/
	bool closeAccess(int);
	/**
	* \brief Parametreli constructor fonksiyon
	*/
	RobotControl(PioneerRobotAPI *robotAPI);
	/**
	* \brief Default constructor
	*/
	RobotControl();
	/**
	* \brief Default destructor
	*/
	~RobotControl();
};

