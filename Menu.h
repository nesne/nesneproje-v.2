#pragma once
#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include "PioneerRobotAPI.h"
#include "RobotControl.h"
/**
* @File Menu.h
* @Date Aralik, 2018
* @Author Serhat gun
*
* \brief Menu class
* \brief Bu class robotun hareketleri icin menu olusturur.
*/

using namespace std;


class Menu
{
	RobotControl *robotControl;
	PioneerRobotAPI* pioneer;
	boolean connectionStatus;

public:
	/**
	* \brief Menu sinifinin constructor fonksiyonu
	*/
	Menu();
	/**
	* \brief Girdi t�rleri icin kontrol fonksiyonu
	* \param menuRange integer tipinde de�isken
	* \param input string tipinde de�isken
	*/
	int controlInput(int menuRange, string input);
	/**
	* \brief Menu sayfasini ekrana yazdirir ve kullanicinin secimine gore robotu kontrol eder
	*/
	void mainMenu();
	/**
	* \brief Bu fonksiyon baglanti menusunu ekrana yazdirir ve kullanicinin gerceklestirecegi islemi sorar.
	*/
	void connectionMenu();
	/**
	* \brief Bu fonksiyon hareket menusunu ekrana yazdirir ve kullanicinin gerceklestirecegi islemi sorar. 
	*/
	void motionMenu();
	/**
	* \brief Bu fonksiyon erisimler icin izin ister.
	*/
	void accessMenu();
	/**
	* \brief Bu fonksiyon yol icin secenekleri gosterir.
	*/
	void pathMenu();
	/**
	* \brief Bu fonksiyon giris icin sifre ister.
	*/
	int askCode();
	/**
	* \brief Bu fonksiyon kullanicidan hiz degerini alir ve dondurur.
	* \return float hiz degiskeni dondurur
	*/
	float askSpeed();
	/**
	* \brief Bu fonksiyon string parametresini float parametresine donusturur.
	* \param input: string
	* \return data : float
	*/
	float controlFloat(string input);
	/**
	* \brief Bu fonksiyon string degerini integer degerine cevirir.
	* \param input : string
	* \return data : integer
	*/
	int controlInteger(string input);
	/**
	* \brief bu fonksiyon hareket hizinin degismemesi icin
	*/
	int askSafeMoveSpeed();
	/**
	* \brief mesafeyi ister.
	*/
	float askDistance();
	/**
	* \brief menu cllassinin yikici fonksiyonu
	*/
	~Menu();
};