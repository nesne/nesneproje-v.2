#include "RobotControl.h"

void RobotControl::turnLeft() {
	if (this->accesState)
	{
		this->robotInterface->turnLeft();
		cout << "Turning Left" << endl;
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
}

void RobotControl::turnRight() {
	if (this->accesState)
	{
		this->robotInterface->turnRight();
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}

}

void RobotControl::forward(float speed) {
	this->robotInterface->forward(speed);
	if (this->accesState)
	{
		this->robotInterface->forward(speed);
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
}

void RobotControl::print() {
	if (this->accesState)
	{
		this->robotInterface->print();
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
}

void RobotControl::backward(float speed) {
	if (this->accesState)
	{
		this->robotInterface->backward(speed);
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
}

Pose RobotControl::getPose() {
	if (this->accesState)
	{
		return *position;
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
}


void RobotControl::setPose(Pose pose) {
	if (this->accesState)
	{
		this->robotInterface->setPose(pose);
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}

}

void RobotControl::stopTurn() {
	if (this->accesState)
	{
		this->robotInterface->stopTurn();
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}

}

void RobotControl::stopMove() {
	if (this->accesState)
	{
		this->robotInterface->stopMove();
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}

}

bool RobotControl::addToPath()
{
	if (this->accesState)
	{
		*this->position = this->robotInterface->getPose();
		this->path->addPos(*this->position);
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
	}
	return true;
}

bool RobotControl::clearPath()
{
	if (this->accesState)
	{
		delete this->path;

		this->path = new Path();
		return true;
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
		return false;
	}

}

bool RobotControl::recordPathToFile()
{
	if (this->accesState)
	{
		int i = 0;
		Pose *pose = this->path->operator[](i);
		string title = "";
		while (pose != NULL)
		{
			title += (to_string(pose->getX())+ "  " + to_string(pose->getY())+ "  " + to_string(pose->getTh())) + "\n";
			
			i++;
			pose = this->path->operator[](i);
		}
		this->record->writeLine(title);
		return true;
	}
	else
	{
		cout << "Unathorized Access" << endl;
		Sleep(2000);
		return false;
	}
}

bool RobotControl::openAccess(int code)
{
	if (!this->accesState)
	{
		if (this->robotOperator->checkAccessCode(code))
		{
			this->accesState = true;
			cout << "Access Opened" << endl;
			Sleep(2000);
			return true;
		}
		else {
			cout << "Wrong Code Violation" << endl;
			Sleep(2000);
			return false;
		}
	}
	else {
		cout << "Already Have Access" << endl;
		Sleep(2000);
		return false;
	}


}

bool RobotControl::closeAccess(int code)
{
	if (this->accesState)
	{
		if (this->robotOperator->checkAccessCode(code))
		{
			this->accesState = false;
			cout << "Access Closed" << endl;
			Sleep(2000);
			return true;
		}
		else
		{
			cout << "Wrong Code Violation" << endl;
			Sleep(2000);
			return false;
		}
	}
	else {
		cout << "No Access Already" << endl;
		Sleep(2000);
		return false;
	}

}

RobotControl::RobotControl()
{

}

RobotControl::RobotControl(PioneerRobotAPI *robot)
{
	this->accesState = false;
	this->robotAPI = robot;
	this->position = new Pose();
	this->path = new Path();
	this->record = new Record();
	this->robotOperator = new RobotOperator();
	this->robotInterface = new PioneerRobotInterface(robot);
	this->sensors.push_back(new LaserSensor(robot));
	this->sensors.push_back(new SonarSensor(robot));
}

RobotControl::~RobotControl()
{
}
