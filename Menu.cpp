#include "Menu.h"
#include <string>

using namespace std;

Menu::Menu()
{
	this->pioneer = new PioneerRobotAPI();
	this->robotControl = new RobotControl(this->pioneer);
	mainMenu();
}

int Menu::controlInput(int menuRange, string input)
{
	if (menuRange < 10)
	{
		if (input.length() > 1)
		{
			return 0;
		}
		else if (isdigit(input.at(0)))
		{
			int choice = atoi(input.c_str());
			if (choice > 0 && choice <= menuRange)
			{
				return choice;
			}
			else
			{
				return 0;
			}
		}
	}
	else
	{
		int choice;
		try
		{
			choice = atoi(input.c_str());
		}
		catch (const std::exception&)
		{
			return 0;
		}
		return choice;
	}
	return 0;
}


void Menu::mainMenu()
{
	while (true)
	{
		string input;
		cout << "Main Menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Access" << endl;
		cout << "4. Path" << endl;
		cout << "5. Quit" << endl;
		cout << "Choose one : ";
		cin >> input;
		int choice = controlInput(5, input);
		switch (choice)
		{
		case 1:
			system("cls");
			cout << " Main Menu -> ";
			connectionMenu();
			break;
		case 2:
			system("cls");
			cout << "Main Menu -> ";
			motionMenu();
			break;
		case 3:
			system("cls");
			cout << "Main Menu -> ";
			accessMenu();
			break;
		case 4:
			system("cls");
			cout << "Main Menu -> ";
			pathMenu();
			break;
		case 5:
			cout << "Quitting the program " << endl;
			Sleep(200);
			return;
		default:
			cout << "Please enter input between 1 and 5" << endl;
			system("cls");
			break;
		}
	}
}
void Menu::connectionMenu()
{
	while (true)
	{
		string input;
		cout << "Connection Menu" << endl;
		cout << "1. Connect Robot" << endl;
		cout << "2. Disconnect Robot" << endl;
		cout << "3. Back" << endl;
		cout << "Choose one : ";
		cin >> input;
		int choice = controlInput(3, input);
		switch (choice)
		{
		case 1:

			if (!connectionStatus)
			{
				if (!pioneer->connect())
				{
					cout << "Could not connect..." << endl;
					cout << "Please open mobile sim if mobile sim opened that means unknown error" << endl;
					Sleep(2000);
					system("cls");
					return;
				}
				else
				{
					connectionStatus = true;
					cout << "Robot Connection Established" << endl;
					Sleep(2000);
					system("cls");
				}
			}
			else
			{
				cout << "Robot Connection Already Established" << endl;
				Sleep(2000);
				system("cls");
			}
			break;
		case 2:
			if (connectionStatus)
			{
				if (!pioneer->disconnect())
				{
					cout << "Robot connection could not closed.Seems have Mobile Sim has a problem" << endl;
					Sleep(2000);
					system("cls");
				}
				else
				{
					cout << "Robot connection closed" << endl;
					connectionStatus = false;
					Sleep(2000);
					system("cls");
					exit(0);
				}
			}
			else
			{
				cout << "Robot connection already closed" << endl;
				Sleep(2000);
				system("cls");
			}
			break;
		case 3:
			system("cls");
			return;
		default:
			cout << "Please enter input between 1 and 3" << endl;
			Sleep(2000);
			system("cls");
			break;
		}
	}
}
void Menu::motionMenu()
{
	while (true)
	{
		if (connectionStatus)
		{
			string input;
			cout << "Motion Menu" << endl;
			cout << "1.  Move Robot" << endl;
			cout << "2.  Turn Left" << endl;
			cout << "3.  Turn Right" << endl;
			cout << "4.  Forward" << endl;
			cout << "5.  Backward" << endl;
			cout << "6.  Print Data" << endl;
			cout << "7.  Stop Robot" << endl;
			cout << "8.  Exit" << endl;
			cout << "Choose one : ";
			cin >> input;
			int choice = controlInput(10, input);
			float speedFloat = 0;
			int speedInteger = 0;
			float angle = 0;
			float distance = 0;
			switch (choice)
			{
			case 1:
				speedFloat = askSpeed();
				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(2000);
				}
				else
				{
					robotControl->forward(speedFloat);
					system("cls");
				}
				break;
			case 2:
				robotControl->turnLeft();
				system("cls");
				break;
			case 3:
				robotControl->turnRight();
				system("cls");
				break;
			case 4:

				speedFloat = askSpeed();

				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(2000);
				}
				else
				{
					robotControl->stopTurn();
					robotControl->forward(speedFloat);
					Sleep(2000);
					system("cls");
					break;
				}
				break;
			case 5:
				speedFloat = askSpeed();

				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(2000);
				}
				else
				{
					robotControl->backward(speedFloat);
					Sleep(2000);
					system("cls");
					break;
				}
				break;
			case 6:
				//robotCon->print();
				Sleep(2000);
				system("cls");
				break;
			case 7:
				robotControl->stopMove();
				cout << "Robot stopped" << endl;
				Sleep(2000);
				system("cls");
				break;
			case 8:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 10" << endl;
				Sleep(2000);
				system("cls");
				break;
			}
		}
		else
		{
			system("cls");
			cout << "Robot connection closed please open connection first" << endl;
			Sleep(2000);
			system("cls");
			return;
		}
	}
}
void Menu::accessMenu()
{
	while (true)
	{
		if (connectionStatus)
		{
			string input;
			cout << "Access Menu" << endl;
			cout << "1.  Open Access" << endl;
			cout << "2.  Close Access" << endl;
			cout << "3.  Exit" << endl;
			cout << "Choose one : ";
			int code = 0;
			cin >> input;
			int choice = controlInput(3, input);
			switch (choice)
			{
			case 1:
				code = askCode();
				this->robotControl->openAccess(code);
				system("cls");
				break;
			case 2:
				code = askCode();
				this->robotControl->closeAccess(code);
				system("cls");
				break;
			case 3:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 3" << endl;
				Sleep(2000);
				system("cls");
				break;
			}
		}
		else
		{
			system("cls");
			cout << "Robot connection closed please open connection first" << endl;
			Sleep(2000);
			system("cls");
			return;
		}
	}
}
void Menu::pathMenu()
{
	while (true)
	{
		if (connectionStatus)
		{
			string input;
			cout << "Motion Menu" << endl;
			cout << "1.  Add Current Position to Path" << endl;
			cout << "2.  Clear Path" << endl;
			cout << "3.  Write Path to File" << endl;
			cout << "4.  Exit" << endl;
			cout << "Choose one : ";
			int code = 0;
			cin >> input;
			int choice = controlInput(4, input);
			switch (choice)
			{
			case 1:
				this->robotControl->addToPath();
				cout << "Path Added" << endl;
				Sleep(2000);
				system("cls");
				break;
			case 2:
				this->robotControl->clearPath();
				cout << "Path Emptied" << endl;
				Sleep(2000);
				system("cls");
				break;
			case 3:
				this->robotControl->recordPathToFile();
				cout << "Record Operation Successfull " << endl;
				Sleep(2000);
				system("cls");
				break;
			case 4:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 4" << endl;
				Sleep(2000);
				system("cls");
				break;
			}
		}
		else
		{
			system("cls");
			cout << "Robot connection closed please open connection first" << endl;
			Sleep(2000);
			system("cls");
			return;
		}
	}
}
int Menu::askCode()
{
	string input;
	int speed;
	cout << "Enter code :" << endl;
	cin >> input;
	speed = controlInteger(input);
	return speed;
}

float Menu::askDistance()
{
	string input;
	float distance;
	cout << "Enter distance :" << endl;
	cin >> input;
	distance = controlFloat(input);
	return distance;
}
int Menu::askSafeMoveSpeed()
{
	string input;
	int speed;
	cout << "Enter speed :" << endl;
	cin >> input;
	speed = controlInteger(input);
	return speed;
}
float Menu::askSpeed()
{
	string input;
	float speed;
	cout << "Enter speed :" << endl;
	cin >> input;
	speed = controlFloat(input);
	return speed;
}
float Menu::controlFloat(string input)
{
	float data;
	try
	{
		data = stof(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return data;
}
int Menu::controlInteger(string input)
{
	int data;
	try
	{
		data = stoi(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return data;
}
Menu::~Menu()
{
}