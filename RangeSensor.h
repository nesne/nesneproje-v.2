#pragma once
#include <string>
#include "PioneerRobotAPI.h"
using namespace std;
/**
* @File RangeSensor.h
* @Date Aralik, 2018
* @Author Seda Kaynar (sedakynr96@gmail.com)
*
* \brief RangeSensor class
* \brief Bu class sensorler icin arayuz niteliginde bir abstract classtir.
*/
class RangeSensor
{
private:
	float *ranges;
	PioneerRobotAPI *robotAPI;
public:
	/**
	* \brief Default constructor
	*/
	RangeSensor();
	/**
	* \brief Parametreli constructor fonksiyon
	*/
	RangeSensor(PioneerRobotAPI *robotAPI);
	/**
	* \brief   Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	* \param float ranges[] : menzil degerlerini dizide tutar.
	*/
	virtual void updateSensor(float *ranges) = 0;
	/**
	* \brief  İndeksin minimum bilgisini dondurur.
	* \param int index : konum
	* \return minimum degeri verir.
	*/
	virtual float getMin(int &index) = 0;
	/**
	* \brief  İndeksin maximum bilgisini dondurur.
	* \param int index : konum
	* \return maximum degeri verir.
	*/
	virtual float getMax(int &index) = 0;
	/**
	* \brief  i. İndeksine sahip sensorun mesafe bilgisini dondurur.
	* \param int index : konum
	* \return menzil bilgisini dondurur.
	*/
	virtual float getRange(int index) = 0;
	/**
	* \brief   indeksi verilen sensor degerini dondurur. getRange(i) ile benzer fonksiyonu gercekler.
	* \param int i : ranges sayi degeri
	* \return ranges[i] degerini dondurur.
	*/
	float operator[](int i);
	/**
	* \brief İndeksi verilen sensorun acı degerini dondurur.
	* \param int index : konum
	*/
	virtual float getAngle(int index) = 0;
	/**
	* \brief startAngle ve endAngle aciları arasinda kalan mesafelerden en kucuk olaninin acisinin angle uzerinde, mesafeyi ise return ile dondurur.
	* \param float startAngle : ilk aci float endAngle : son aci float, angle : aci degeri
	*/
	virtual float getClosestRange(float startAngle, float endAngle, float& angle) = 0;
	/**
	* \brief laserSensor classinin yikici fonksiyondur.
	*/
	~RangeSensor();
};