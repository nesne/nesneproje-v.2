#pragma once
#include "RangeSensor.h"

/**
* @File LaserSensor.h
* @Date Aralik, 2018
* @Author Seda Kaynar (sedakynr96@gmail.com)
*
* \brief LaserSensor Sinifi
*/

class LaserSensor : public RangeSensor
{
private:
	PioneerRobotAPI *robotAPI;
public:
	LaserSensor(PioneerRobotAPI *robotAPI) : RangeSensor(robotAPI) {
		this->robotAPI = robotAPI;
	};
	/**
	* \brief  i. İndeksine sahip sensorun mesafe bilgisini dondurur.
	* \param int index : konum
	* \return menzil bilgisini dondurur.
	*/
	float getRange(int index);
	/**
	* \brief   Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	* \param float ranges[] : menzil degerlerini dizide tutar.
	*/
	void updateSensor(float *ranges);
	/**
	* \brief  İndeksin maximum bilgisini dondurur.
	* \param int index : konum
	* \return maximum degeri verir.
	*/
	float getMax(int &index);
	/**
	* \brief  İndeksin minimum bilgisini dondurur.
	* \param int index : konum
	* \return minimum degeri verir.
	*/
	float getMin(int &index);
	/**
	* \brief   indeksi verilen sensor degerini dondurur. getRange(i) ile benzer fonksiyonu gercekler.
	* \param int i : ranges sayi degeri
	* \return ranges[i] degerini dondurur.
	*/
	/*float operator[](int i);*/
	/**
	* \brief İndeksi verilen sensorun acı degerini dondurur.
	* \param int index : konum
	*/
	float getAngle(int index);
	/**
	* \brief startAngle ve endAngle aciları arasinda kalan mesafelerden en kucuk olaninin acisinin angle uzerinde, mesafeyi ise return ile dondurur.
	* \param float startAngle : ilk aci float endAngle : son aci float, angle : aci degeri
	*/
	float getClosestRange(float startAngle,float endAngle,float &angle);
	/**
	* \brief laserSensor classinin yapici fonksiyondur.
	*/
	LaserSensor();
	/**
	* \brief laserSensor classinin yikici fonksiyondur.
	*/
	~LaserSensor();
};

