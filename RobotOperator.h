#pragma once
/**
* @File RobotOperator.h
* @Date Aralik, 2018
* @Author Eda Ayaz
*
* \brief Robot operator Sinifi
*/
#include <string>
#include <iostream>
#include "Encryption.h"
using namespace std;

class RobotOperator
{
public:
	RobotOperator();
	~RobotOperator();

	bool checkAccessCode(int);
	void print();

private:
	Encryption * enc;
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	/**
	* \brief 4 rakamdan olu�an kodu, Encryption s�n�f�n�n fonksiyonu kullan�larak
	�ifrelendirir.
	*/
	int encryptCode(int);
	/**
	* \brief 4 rakamdan olu�an kodu, Encryption s�n�f�n�n fonksiyonu kullan�larak �ifresini
	��zd�r�r.
	*/
	int decryptCode(int);
};

