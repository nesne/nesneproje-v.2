#include "RobotOperator.h"
using namespace std;

RobotOperator::RobotOperator()
{
	int sifre = 1234;
	this->accessCode = this->encryptCode(sifre);
	this->accessState = false;
}


RobotOperator::~RobotOperator()
{
}

bool RobotOperator::checkAccessCode(int girilen_sifre)
{
	int code;
	code = enc->encryption(girilen_sifre);
	if (code == this->accessCode)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void RobotOperator::print()
{
	cout << name << " " << surname << " " << endl;
}

int RobotOperator::encryptCode(int sifre)
{
	int code;
	code = enc->encryption(sifre);

	return code;
}

int RobotOperator::decryptCode(int sifre)
{
	int coz;
	coz = enc->encryption(sifre);

	return coz;
}