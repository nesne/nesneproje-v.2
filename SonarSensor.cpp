#include "RangeSensor.h"
#include "SonarSensor.h"

float SonarSensor::getClosestRange(float startAngle, float endAngle, float & angle)
{
	//switch (index)
	//{
	//case 0:
	//case 15:
	//	return -90;
	//case 1:
	//case 14:
	//	return -50;
	//case 2:
	//case 13:
	//	return -30;
	//case 3:
	//case 12:
	//	return -10;
	//case 4:
	//case 11:
	//	return 10;
	//case 5:
	//case 10:
	//	return 30;
	//case 6:
	//case 9:
	//	return 50;
	//case 7:
	//case 8:
	//	return 90;
	//default:
	//	return -1;
	//	break;
	//}

	// Umimplemented Method
	return 0.00f;
}
SonarSensor::SonarSensor()
{
}

float SonarSensor::getRange(int index)
{
	return this->operator[](index);
}

float SonarSensor::getMax(int& index)
{
	float maximumValue = this->operator[](0);
	for (int i = 0; i < 16; i++)
	{
		if (maximumValue < this->operator[](0))
		{
			maximumValue = this->operator[](0);
		}
	}
	return maximumValue;
}

float SonarSensor::getMin(int& index)
{
	float minimumValue = this->operator[](0);
	for (int i = 0; i < 16; i++)
	{
		if (minimumValue > this->operator[](0))
		{
			minimumValue = this->operator[](0);
		}
	}
	return minimumValue;
}
void SonarSensor::updateSensor(float *ranges)
{
	this->robotAPI->getSonarRange(ranges);
}
float SonarSensor::getAngle(int index)
{
	switch (index)
	{
	case 0:
	case 15:
		return -90;
	case 1:
	case 14:
		return -50;
	case 2:
	case 13:
		return -30;
	case 3:
	case 12:
		return -10;
	case 4:
	case 11:
		return 10;
	case 5:
	case 10:
		return 30;
	case 6:
	case 9:
		return 50;
	case 7:
	case 8:
		return 90;
	default:
		return -1;
		break;
	}
}

SonarSensor::~SonarSensor()
{
}
