#pragma once
/**
* @File Encryption.h
* @Date Aralik, 2018
* @Author Eda Ayaz
*
* \brief Encryption class
* \brief Bu class sifrelemeyi saglar
*/
class Encryption
{
public:
	/**
	* \brief Default constructor
	*/
	Encryption();
	/**
	* \brief Default destructor
	*/
	~Encryption();
	/**
	* \brief 4 rakamli sayilarin sifrelenmesini yapmaktadir
	*/
	int encryption(int);
	/**
	* \brief 4 rakamli sayilarin sifre cozumunu yapmaktadir.
	*/
	int decryption(int);
};

