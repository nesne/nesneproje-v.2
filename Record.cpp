#include "Record.h"
#include <iostream>
using namespace std;


Record::Record()
{
	this->fileName = "deneme.txt";
}


Record::~Record()
{
}

bool Record::openFile(string fileName)
{
	this->fileName = fileName;
	this->file.open(fileName, fstream::out);

	return true;
}

bool Record::closeFile()
{
	file.close();
	return true;
}

void Record::setFileName(string name)
{
	this->fileName = name;
}

string Record::readLine()
{
	this->file.open(this->fileName);

	string line;
	getline(this->file, line);

	file.close();
	return line;
}

bool Record::writeLine(string str)
{
	this->file.open(this->fileName, std::ofstream::out | std::ofstream::trunc);

	this->file << str;
	this->file.close();

	return true;
}

Record& Record::operator<<(string str)
{
	this->file.open(this->fileName, std::ofstream::out | std::ofstream::trunc);

	this->file << str << endl;
	this->closeFile();
	return *this;
}
string& Record::operator>>(string str)
{
	this->file.open(fileName);
	getline(this->file, str);

	return str;
}
