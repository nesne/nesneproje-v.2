#include "LaserSensor.h"

LaserSensor::LaserSensor()
{
}
float LaserSensor::getRange(int index)
{
	return this->operator[](index);
}

void LaserSensor::updateSensor(float *ranges)
{
	this->robotAPI->getLaserRange(ranges);
}
float LaserSensor::getMax(int &index)
{
	int max = this->operator[](0);
	for (int i = 1; i < 16; i++)
	{
		if (this->operator[](i) > max)
		{
			max = this->operator[](i);
			index = i;
		}
	}
	return max;
	return 0.00f;
}
float LaserSensor::getMin(int &index)
{
	int min = this->operator[](0);
	for (int i = 1; i < 16; i++)
	{

		if (this->operator[](i) < min)
		{
			min = this->operator[](i);
			index = i;
		}
	}
	return min;
}

float LaserSensor::getAngle(int index)
{
	if (index >= 0 && index <= 180)
	{
		return  index - 90;
	}
	else
	{
		// Yanlıs sayı girdisi
	}
	return 0.00f;
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	if (startAngle > 90 && endAngle < -90)
	{
		int startIndex = startAngle + 90;
		int endIndex = endAngle + 90;
		if (startIndex > endIndex)
		{
			int temp = startIndex;
			startIndex = endIndex;
			endIndex = temp;
		}

		int min = this->operator[](startIndex);
		for (int i = startIndex + 1; i < endIndex; i++)
		{
			if (min > this->operator[](startIndex)) {
				min = this->operator[](i);
			}
		}
		return min;
	}
	else
	{
		return 0;
		// Yanlış Açı girdisi
	}
	return 0.00f;
}

LaserSensor::~LaserSensor()
{
}
