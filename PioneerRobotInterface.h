#pragma once
#include "RobotInterface.h"
/**
* @File PioneerInterface.h
* @Date Aralik, 2018
* @Author Serhat gun (serhatgunee@gmail.com)
*
* \brief PioneerInterface class
* \brief Bu class abstract bir sinif olup, PioneerRobotAPI ile dogrudan iliskili olan PineerRobotInterface icin bir arayuz olusturmaktad�r.
*/
class PioneerRobotInterface : public RobotInterface
{
private:
	PioneerRobotAPI * robotAPI;
public:
	PioneerRobotInterface(PioneerRobotAPI *robotAPI) : RobotInterface(robotAPI) {
		this->robotAPI = robotAPI;
	};
	/**
	* \brief Robotu sola donduren fonksiyon
	*/
	void turnLeft();
	/**
	* \brief Robotu sola donduren fonksiyon
	*/
	void turnRight();
	/**
	* \brief Robotun verilen hizda ilerlemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void forward(float speed);
	/**
	* \brief Konumu, laser ve sonar sensorleri konsola yazdirir
	*/
	void print();
	/**
	* \brief Robotun verilen hizda gerilemesini saglayan fonksiyon
	* \param speed: hiz degiskeni
	*/
	void backward(float speed);
	/**
	* \brief Pozisyonu almamizi saglayan fonksiyon
	* \return Pose* tipinde pozisyonu dondurur
	*/
	Pose getPose();
	/**
	* \brief Pozisyona deger atamamizi saglayan fonksiyon
	* \param Pozisyon tutan Pose
	*/
	void setPose(Pose);
	/**
	* \brief Robotun donusunu durduran fonksiyon
	*/
	void stopTurn();
	/**
	* \brief Robotu durduran fonksiyon
	*/
	void stopMove();
	/**
	* \brief   Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	* \param float ranges[] : menzil degerlerini dizide tutar.
	*/
	void updateSensor();
	/**
	* \brief Default destructor
	*/
	~PioneerRobotInterface();
};

