#include "PioneerRobotInterface.h"

void PioneerRobotInterface::turnLeft()
{
	this->robotAPI->turnRobot(robotAPI->left);
}

void PioneerRobotInterface::turnRight()
{
	this->robotAPI->turnRobot(robotAPI->right);
}

void PioneerRobotInterface::forward(float speed)
{
	this->robotAPI->moveRobot(speed);
}

void PioneerRobotInterface::print()
{
	/*this->robotAPI->*/
}

void PioneerRobotInterface::backward(float speed)
{
	speed *= -1;
	this->robotAPI->moveRobot(speed);
}

Pose PioneerRobotInterface::getPose()
{
	Pose *pose = new Pose();
	pose->setX(this->robotAPI->getX());
	pose->setY(this->robotAPI->getY());
	pose->setTh(this->robotAPI->getTh());
	return *pose;
}

void PioneerRobotInterface::setPose(Pose pose)
{
	this->robotAPI->setPose(pose.getX(), pose.getY(), pose.getTh());
}

void PioneerRobotInterface::stopTurn()
{
	this->robotAPI->turnRobot(this->robotAPI->forward);
}

void PioneerRobotInterface::stopMove()
{
	this->robotAPI->stopRobot();
}

void PioneerRobotInterface::updateSensor()
{
	//this->updateSensor();
}

PioneerRobotInterface::~PioneerRobotInterface()
{
	delete this->robotAPI;
}
